﻿/*console*/
$(document).ready(function ($) {
    "use strict";

    // loading
    //$(window).on('load', function () {
    //    if ($('.pre-load').length) {
    //        $('.pre-load').delay(200).fadeOut(500);
    //    }
    //});

    $("ul.menu__links li").click(function (e) {
        // e.preventDefault();
        $(this).children("ul.menu__links__sup").toggleClass("active");
        $("ul.menu__links__sup").not($(this).children("ul.menu__links__sup")).removeClass("active");
    });


    // Click on video thumbnail or link
    $(".overlay").on("click", function (e) {

        // prevent default behavior for a-tags, button tags, etc. 
        e.preventDefault();

        // Grab the video ID from the element clicked
        var id = $(this).parents(".info__item__img").children(".open-video").attr('data-youtube-id');
        console.log(id);

        // Autoplay when the modal appears
        // Note: this is intetnionally disabled on most mobile devices
        // If critical on mobile, then some alternate method is needed
        var autoplay = '?autoplay=1';

        // Don't show the 'Related Videos' view when the video ends
        var related_no = '&rel=0';

        // String the ID and param variables together
        var src = 'https://www.youtube.com/embed/' + id + autoplay + related_no;

        // Pass the YouTube video ID into the iframe template...
        // Set the source on the iframe to match the video ID
        $("#youtube").attr('src', src);

        // Add class to the body to visually reveal the modal
        //   $("body").addClass("show-video-modal noscroll");

    });

    // mainImgSlider

    $('.mainImgSlider').owlCarousel({
        loop: true,
        margin: 25,
        items: 2,
        rtl: true,
        autoplay: true,
        animate: true,
        dots: true,
        navText: [
            "<i class='fa fa-angle-right' aria-hidden='true'></i>",
            "<i class='fa fa-angle-left' aria-hidden='true'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });
    if ($('.scroll').length > 0) {

        $(".scroll").niceScroll({
            // autohidemode: false,
            cursorcolor: '#00b2c3'
        });
    }


    // slider
    $('.owl-carousel-news').owlCarousel({
        loop: true,
        margin: 25,
        items: 2,
        rtl: true,
        autoplay: true,
        // animate: true,
        navText: [
            "<i class='fa fa-angle-right' aria-hidden='true'></i>",
            "<i class='fa fa-angle-left' aria-hidden='true'></i>"
        ],
        responsive: {
            0: {
                items: 1
            },
            750: {
                items: 1
            },
            1000: {
                items: 1
            }
        }
    });

    if ($('#datepicker').length) {
        $('#datepicker').datepicker({
            uiLibrary: 'bootstrap4'
        });
    }

    // show and hide voting
    var mainSideMenuHelp = $('.mainSideMenuHelp')

    $('.votingAction').click(function (e) {
        e.preventDefault();
        mainSideMenuHelp.toggleClass('active');
        if (mainSideMenuHelp.hasClass('active')) {
            mainSideMenuHelp.animate({
                width: '245px'
            }, 0);
            $('.votingChoose').show();
            $('.votingChooseDetails').hide();
            $('.newsLetter').hide()
            $('.shareGroup').hide();
            $('.showVoting').show();

        } else {
            mainSideMenuHelp.animate({
                width: '0'
            }, 0);
            $('.votingChoose').hide();
            $('.shareGroup').hide();
            $('.votingChooseDetails').show();

        }
    });

    // show and hide newsLetter
    $('.newsLetterAction').click(function (e) {

        e.preventDefault();
        mainSideMenuHelp.toggleClass('active');
        if (mainSideMenuHelp.hasClass('active')) {
            mainSideMenuHelp.animate({
                width: '245px'
            }, 0);
            $('.showVoting').hide();
            $('.shareGroup').hide();
            $('.newsLetter').show()
        } else {
            mainSideMenuHelp.animate({
                width: '0'
            }, 0);
            $('.showVoting').show();
            $('.newsLetter').hide();
            $('.shareGroup').hide();

        }
    });
    // show and hide newsLetter
    $('.shareAction').click(function (e) {

        e.preventDefault();
        mainSideMenuHelp.toggleClass('active');
        if (mainSideMenuHelp.hasClass('active')) {
            mainSideMenuHelp.animate({
                width: '245px'
            }, 0);
            $('.showVoting').hide();
            $('.newsLetter').hide()
            $('.shareGroup').show();
        } else {
            mainSideMenuHelp.animate({
                width: '0'
            }, 0);
            $('.showVoting').show();
            $('.newsLetter').hide()
        }
    });

    // show voting details
    $('.btnVotingAction').click(function () {
        $('.votingChoose').hide();
        $('.votingChooseDetails').show();
    });


    // nav 
    $(".click--nav").click(function () {
        $(this).toggleClass("open");
        $(".click--nav").toggleClass("active");
        $(".body-overlay").toggleClass("back");
        $(".nav").toggleClass("back");
        $('body').css({
            'overflow-y': 'hidden'
        });
    });


    $(".body-overlay").click(function () {
        $(".click--nav").removeClass("open");
        $(".click--nav").removeClass("active");
        $(".body-overlay").removeClass("back");
        $(".nav").removeClass("back");
        $('body').css({
            'overflow-y': 'auto'
        });
    });


    // page training
    $('input:radio[name="radio"]').change(function () {
        if ($(this).val() == 'training') {
            $(".training__div").slideToggle(250);
            $(".degree__div").slideToggle(250);
        } else if (($(this).val() == 'degree')) {
            $(".training__div").slideToggle(250);
            $(".degree__div").slideToggle(250);
        }
    });


    // active career profile 
    $('.education__level__fund ul li').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });

    // change color 
    $('.option__search .ico__media').click(function () {
        $('body').toggleClass('changeColor');
    });

    // Change color On  
    $('.font_p_header .ico__header__big').click(function () {
        $(this).addClass('active').siblings().removeClass('active');
    });



    // change font
    var fontLi = $(".decreaseFont,.increaseFont,.normalFont");
    fontLi.click(function () {
        $("link[href*='fontSize']").attr("href", $(this).attr("data-value"));
    });

    // show and hidden  
    $('.noBtn').click(function () {
        $('.hiddenGroup').addClass('active');
        $(this).attr("checked", "checked");
        $('.yesBtn').attr("checked", "unchecked")
    });
    $('.yesBtn').click(function () {
        $('.hiddenGroup').removeClass('active')
        $(this).attr("checked", "checked");
        $('.noBtn').attr("checked", "unchecked")

    });

    // show and hidden password 
    var btnShowPassword = $('.btnShowPassword')
    btnShowPassword.click(function () {
        var x = $('.myPasswordInput');
        if (x.attr("type") == "password") {
            x.attr("type", "text");
        } else {
            x.attr("type", "password");
        }
        console.log(x)
    });
});

function SucssesMsg(title, body) {

    swal(title, body, "success")
}

window.onload = function () {
    $('.pre-load').fadeOut(500);
}


function share() {
    var mainSideMenuHelp = $('.mainSideMenuHelp');
    mainSideMenuHelp.toggleClass('active');
    if (mainSideMenuHelp.hasClass('active')) {
        mainSideMenuHelp.animate({
            width: '245px'
        }, 0);
        $('.showVoting').hide();
        $('.newsLetter').hide()
        $('.shareGroup').show();
    } else {
        mainSideMenuHelp.animate({
            width: '0'
        }, 0);
        $('.showVoting').show();
        $('.newsLetter').hide()
    }
}

//function showPassword() {
//    var x = document.getElementsByClassName('myPasswordInput.ClientID');
//    if (x.type === "password") {
//        x.type = "text";
//    } else {
//        x.type = "password";
//    }
//    console.log(x.type);
//}