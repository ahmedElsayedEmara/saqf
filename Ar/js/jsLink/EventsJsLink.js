/// <reference path="E:\Program Files\Common Files\Microsoft Shared\Web Server Extensions\15\TEMPLATE\LAYOUTS\clienttemplates.js" />
(function () {
    (window.jQuery || document.write('<script src="/_layouts/15/EEC_Branding/ar-sa/js/framworks/jquery-3.3.1.js"></script>'));
    var newCtx = {};
    newCtx.Templates = {};
    newCtx.ListTemplateType = 4004;
    newCtx.OnPostRender = onPostRenderTemplate;
    SPClientTemplates.TemplateManager.RegisterTemplateOverrides(newCtx);

    function EventNotLimited() {
        $("span input[id*='AvailableNumber']").closest("tr").hide();
    }

    function EventLimited() {
       
        $("span input[id*='AvailableNumber']").closest("tr").show();
    }

    function changeStatus() {
        var RequestStatusVal = $("select[id*='EventsType']").val();
        if (RequestStatusVal == "محددة" || RequestStatusVal == "Limited") {
            EventLimited();
        }
        else  {
            EventNotLimited();

        }
    }



    function onPostRenderTemplate(ctx) {
       EventNotLimited();
        changeStatus();
        $("select[id*='EventsType']").change(function () {
            changeStatus();
        });
    }

})();


