$(document).ready(function ($) {
    if ($(window).width() > 1200) {
        $(".megaLink").hover(
            function () {
                $('.megaMenu', this).stop(true, true).delay(100).slideDown(200); 
            },
            function () {
                $('.megaMenu', this).stop(true, true).slideUp(10); 
            }
        );
    }
});